let
    toggleOfMouseClick,
    borderColor = '#000000',
    borderType = 'solid',
    markType = 'disc',
    numberType = 'decimal',
    createTable,
    createMarckedList,
    createNumberedList,
    remove,
    valid = [];

const password = ['yes'],
    textArea = getE('#text-area'),
    radioBtn = getAll('.radio-btn'),
    radioTab = getAll('.radio-tab'),
    tabContent = getAll('.tab-content'),
    text = getE('.content div'),
    editorStyleButtons = [].slice.call(getE('.editor-style-panel').children),
    exampleWindow = getE('.example-window'),
    buttonValue = ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'a'];
    
/**
Get element by query selector.
*/
function getE(element) {
    return document.querySelector(element);
}

/**
Get elements by query selector all.
*/
function getAll(element) {
    return document.querySelectorAll(element);
}

/**
Show block.
@param {string} pathToElement
*/
function showElement(pathToElement) {
    getE(pathToElement).style.display = 'flex';
}

/**
Hide block.
@param {string} pathToElement
*/
function hideElement(pathToElement) {
    getE(pathToElement).style.display = 'none';
}

/**
Add style for dicoreting of the text in the text-area.
@param {number} index
*/
function addStyleByClickOnButton(index) {
    textArea.value += ` <${buttonValue[index]}> </${buttonValue[index]}>`;
}


/**
Delete all class "active" from sections with class "tab-content".
*/
remove = function () {
    radioTab.forEach(function (radioTabValue, indexRadioTab) {
        tabContent[indexRadioTab].classList.remove('active');
    });
};

/**
Add class "active" to section with class "tab-content" for making active section visible.
 @param {string} radioTabValue
 @param {number} indexRadioTab
*/
radioTab.forEach(function (radioTabValue, indexRadioTab) {
    passesAllInputInActiveTab();
    radioTabValue.addEventListener('click', function () {
        remove();
        tabContent[indexRadioTab].classList.add('active');
        passesAllInputInActiveTab();
    });
});

radioTab.forEach(function (checkedType) {
    checkedType.addEventListener('click', function () {
        if (getE('.radio-tab').checked) {
            showElement('.tab-content-table');
            hideElement('.tab-content-list');
        } else {
            showElement('.tab-content-list');
            hideElement('.tab-content-table');
        }
    });
});

//radioTab.forEach(function (radioTabValue, index) {
//    workWithActiveTab();
//    showElement('.tab-content-table');
//    hideElement('.tab-content-list');
//    radioTabValue.addEventListener('click', function () {
//        if (index) {
//            hideElement('.tab-content-table');
//            showElement('.tab-content-list');
//        } else {
//            showElement('.tab-content-table');
//            hideElement('.tab-content-list');
//        }
//    });
//});


/**
Create marcked list
*/
createMarckedList = function () {
    let countUl = '';
    const countList = getE('.count-marcked-list').value;
    countUl += ` <ul style="list-style:${markType}"> `;
    for (let i = 0; i < countList; i++) {
        countUl += ` <li>Текст</li> `;
    }
    countUl += ' </ul>';
    return countUl;
}

/**
Create numbered list
*/
createNumberedList = function () {
    let countOl = '';
    const countList = getE('.count-numbered-list').value;
    countOl += ` <ol style="list-style:${numberType}"> `;
    for (let i = 0; i < countList; i++) {
        countOl += ` <li>Текст</li> `;
    }
    countOl += ' </ol>';
    return countOl;
}

/**
Create table
*/
createTable = function () {
    let table = '';
    const countRow = getE('.count-row').value,
        countCell = getE('.count-cell').value,
        heightCell = getE('.height-cell').value,
        widthCell = getE('.width-cell').value,
        widthBorder = getE('.width-border').value;
    table += ' <table> ';
    for (let i = 0; i < countRow; i++) {
        table += `<tr>`;
        for (let j = 0; j < countCell; j++) {
            table += ` <td style="border: ${widthBorder}px ${borderType} ${borderColor}; width:${widthCell}px; height:${heightCell}px"> </td> `;
        }
        table += ' </tr>';
    }
    table += ' </table>';
    return table;
};

/**
Confirmation on validation. It's true if it isn't 'string', 0, bigger then 99 or less then 0.
@param {number} inputValue.
*/
numberIs = function (inputValue) {
    return (!isNaN(inputValue) && !0 && inputValue > 0 && inputValue < 100);
};

/**
Copy from content to text-area.
*/
getE('.button-edit').addEventListener('click', function () {
    showElement('.editor');
    showElement('.editor-area');
    hideElement('.editor-style');
    hideElement('.editor-add');
    textArea.value = getE('.content div').innerHTML;
});

/**
Apeeared or disapeared table style by onclick.
*/
getE('.button-style').addEventListener('click', function () {
    hideElement('.editor-area');
    hideElement('.editor-add');
    showElement('.editor-style');
});

/**
Copy from content to text-area.
*/
getE('.button-save').addEventListener('click', function () {
    text.innerHTML = textArea.value;
});

/**
Appeared or disappeared table add by onclick.
*/
getE('.button-add').addEventListener('click', function () {
    showElement('.editor-add');
    hideElement('.editor-area');
});

/**
Hide the block window if password true, by the click on the unblock button
*/
getE('.button-unblock').addEventListener('click', function () {
    if (getE('.input-password').value === password[0]) {
        hideElement('.password-window');
        hideElement('.layout');
        hideElement('.password-panel div');
        getE('.input-password').value = '';
    } else {
        showElement('.password-panel div');
        getE('.input-password').value = '';
    }
});

/**
Show the block window by the click on the block button
*/
getE('.button-block').addEventListener('click', function () {
    showElement('.password-window');
    showElement('.layout');
});

/**
Change the size of text in text-area.
*/
radioBtn.forEach(function (radioButton) {
    radioButton.addEventListener('click', function () {
        text.style.fontSize = this.value;
    });
});

/**
Change the font of text in text-area.
*/
getE('.text-font select').addEventListener('change', function () {
    text.style.fontFamily = this.value;
});

/**
Change the background.
*/
getE('.style-background-picker').addEventListener('change', function () {
    text.style.background = `${getE('.style-background-picker').value}`;
});

/**
Change the background.
*/
getE('.style-color-picker').addEventListener('change', function () {
    text.style.color = `${getE('.style-color-picker').value}`;
});

/**
Change the font-Weight of text in text-area.
*/
getE('.text-bold').addEventListener('change', function () {
    if (this.checked) {
        text.style.fontWeight = 'bold';
    } else {
        text.style.fontWeight = 'normal';
    }
});

/**
Change the font-Style of text in text-area.
*/
getE('.text-italic').addEventListener('change', function () {
    if (this.checked) {
        text.style.fontStyle = 'italic';
    } else {
        text.style.fontStyle = 'normal';
    }
});

/**
Change the text decoration of text in text-area.
*/
getE('.text-underline').addEventListener('change', function () {
    if (this.checked) {
        text.style.textDecoration = 'underline';
    } else {
        text.style.textDecoration = 'none';
    }
});

/**
Change border-type  by choose.
*/
getE('.border-type select').addEventListener('change', function () {
    borderType = this.value;
});

///**
//Change border-color  by choose.
//*/
getE('.border-color input').addEventListener('change', function () {
    borderColor = this.value;
});

/**
Change mark-type-block  by choose.
*/
getE('.mark-type-block select').addEventListener('change', function () {
    markType = this.value;
});

getE('.number-type-block select').addEventListener('change', function () {
    numberType = this.value;
});

/**
Passes all buttons on text-area and add text decoration by click on some button
*/
editorStyleButtons.forEach(function (styleButton, index) {
    styleButton.addEventListener('click', function () {
        addStyleByClickOnButton(index);
    });
});

/**
Create table or list in text-area
*/
getAll('.button-create').forEach(function (buttonCreat) {
    buttonCreat.addEventListener('click', function () {
        hideElement('.editor-add');
        showElement('.content');
        showElement('.control-panel');
        showElement('.editor-area');
        checkedActiveTabAndCreate();
    });
});

/**
Check checked tab
@return {string} textArea.value
*/
function checkedActiveTabAndCreate() {
    if (getE('.radio-tab').checked) {
        textArea.value += createTable();
    } else if (getE('.list-type-radio').checked) {
        textArea.value += createMarckedList();
    } else {
        textArea.value += createNumberedList();
    }
    return textArea.value;
}



// /**
// Change color of outline on 'green', if the client input right information, and 'red' if it's wrong.
//  // @param {string }inputClassName
// */
// // changeColor = function (inputClassName) {
// //     inputClassName.addEventListener('input', function () {
// //         if (numberIs(this.value)) {
// //             this.style.outlineColor = 'green';
// //             this.style.background = 'rgba(0,128,0,.2)';
// //             this.parentElement.classList.remove('tooltipMessage');
// //         } else {
// //             this.style.outlineColor = 'red';
// //             this.style.background = 'rgba(255,0,0,.2)';
// //             this.parentElement.classList.add('tooltipMessage');
// //             //            console.log(numberIs(this.value));
// //         }
// //     });
// // };


/**
Reset input field by change active tab.
*/
function resetAllInput(inputValue) {
    inputValue.value = '';
    inputValue.style.outlineColor = '#6a707c';
    inputValue.parentElement.classList.remove('tooltipMessage');
    hideElement('.button-create');
}
console.log(getAll('.tab-with-input-field input[type=text]'));
/**
Passes all input fields in order to gain access to the active field and Change color of outline on 'green', if the
 client input right information, and 'red' if it's wrong.
*/
function passesAllInputInActiveTab() {
    let validRow = [];
    getAll('.tab-with-input-field input[type=text]').forEach(function(inputActive, index) {
        resetAllInput(inputActive);
        inputActive.addEventListener('input', function () {
            validRow[index] = showInvalidInput(inputActive, index);
            valid = validRow;
            console.log(valid);
//            console.log(getAll('.count-marcked-list'));
//            console.log(getAll('.count-marcked-list').length);
            validateAndShowCreateButton(getAll('.tab-with-input-field input[type=text]').length)
        });
    });
}


function validateAndShowCreateButton(countOfInputField) {
    let count = 0;
    valid.forEach(function (inputValid, index) {
        if (inputValid) {
            count++;
        } console.log(count);
         console.log(countOfInputField);
        if (count === countOfInputField) {
            showElement('.button-create');
        } else {
            hideElement('.button-create');
        }
    });
}
//function passesAllInputInActiveTab(pathToInput) {
//    getAll(pathToInput).forEach(function (inputActive, index) {
//        inputActive.addEventListener('input', function () {
//            showInvalidInput(inputActive, index);
//        });
//    });
//}
//
//function workWithActiveTab() {
//    if (radioTab[0].checked) {
//        passesAllInputInActiveTab('.active input[type=text]');
//    } else if (getAll('.list-type-radio')[0].checked) {
//        passesAllInputInActiveTab('.count-marcked-list');
////                alert('2');
//    } else if (getAll('.list-type-radio')[1].checked) {
//        passesAllInputInActiveTab('.count-numbered-list');
////                alert('3');
//    }
//}
/**
Show valid or invalid input block
*/
function showInvalidInput(inputValue, index) {
    if (numberIs(inputValue.value)) {
        inputValue.style.outlineColor = 'green';
        inputValue.parentElement.classList.remove('tooltipMessage');
        return true;
    } else {
        inputValue.style.outlineColor = 'red';
        inputValue.parentElement.classList.add('tooltipMessage');
        return false;
    }
}

/**
Show or hide list types by checked. 
*/
getAll('.list-type-radio').forEach(function (checkedType) {
    checkedType.addEventListener('click', function () {
        if (getE('.list-type-radio').checked) {
            showElement('.fieldset-marked-list-types');
            hideElement('.fieldset-numbered-list-types');
        } else {
            showElement('.fieldset-numbered-list-types');
            hideElement('.fieldset-marked-list-types');
        }
    });
});

/**
By the click on the show example button show example of the table
*/
getE('.button-example').addEventListener('click', function () {
    if (getE('.radio-tab').checked) {
        getE('.example-window-body').innerHTML = createTable();
    } else if (getE('.list-type-radio').checked) {
        getE('.example-window-body').innerHTML = createMarckedList();
    } else {
        getE('.example-window-body').innerHTML = createNumberedList();
    }
    //    getE('.example-window-body').innerHTML = createTable();
});

/**
By the click on close icon hide rxample window 
*/
getE('.example-window-head img').addEventListener('click', function () {
    hideElement('.example-window');
    showElement('.example-window-icon img');
});

/**
By the click on the example window icon show this window 
*/
getE('.example-window-icon img').addEventListener('click', function () {
    showElement('.example-window');
    hideElement('.example-window-icon img');
});


exampleWindow.addEventListener('mousedown', function (event) {
    toggleOfMouseClick = true;
});

document.addEventListener('mousemove', function (event) {
    if (toggleOfMouseClick) {
        exampleWindow.style.top = event.clientY - exampleWindow.offsetHeight + 'px';
        exampleWindow.style.left = event.clientX - exampleWindow.offsetWidth + 'px';
    }
});

exampleWindow.addEventListener('mouseup', function () {
    toggleOfMouseClick = false;
});

//exampleWindow.ondragstart = function () {
//    return false;
//};
