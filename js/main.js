let isMouseDown = false,
    borderColor = '#000000',
    borderType = 'solid',
    markType,
    createTable,
    createMarckedList,
    createNumberedList,
    remove,
    active,
    valid = [],
    mouseOffSet = {x: 0, y: 0};

const password = 'yes',
    textArea = getE('#text-area-block'),
    radioBtn = getAll('.radio-btn'),
    radioTab = getAll('.radio-tab'),
    tabContent = getAll('.tab-content'),
    text = getE('.content div'),
    editorStyleButtons = [].slice.call(getE('.editor-style-panel').children),
    exampleWindow = getE('.example-window'),
    buttonValue = ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'a'];

/**
 *Get element by query selector
 *@param {string} element
 *rerturn {htmlElement}
 */
function getE(element) {
    return document.querySelector(element);
}

/**
 *Get elements by query selector all
 *@param {string} element
 *rerturn {htmlElement}
 */
function getAll(element) {
    return document.querySelectorAll(element);
}

/**
 *Show block.
 *@param {array} arrayOfPathes
 *@param {htmlElement} pathToElement
 */
function showElement(arrayOfPathes) {
    arrayOfPathes.forEach(function (pathToElement) {
        getE(pathToElement).style.display = 'flex';
    });
}
/**
 *Hide block
 *@param {array} arrayOfPathes
 *@param {htmlElement} pathToElement
 */
function hideElement(arrayOfPathes) {
    arrayOfPathes.forEach(function (pathToElement) {
        getE(pathToElement).style.display = 'none';
    });
}
/**
 *Validating password
 */
function validatePassword() {
    if (getE('.input-password').value === password) {
        hideElement(['.password-window', '.layout', '.password-panel div']);
        getE('.input-password').value = null;
    } else {
        showElement(['.password-panel div']);
        getE('.input-password').value = null;
    }
}

/**
 *Add style for dicoreting of the text in the text-area-block.
 *@param {number} index
 */
function addStyleByClickOnButton(index) {
    textArea.value += ` <${buttonValue[index]}> </${buttonValue[index]}>`;
}

/**
 *Delete all class with name removedClass
 *@param {htmlElement} pathToRadio
 *@param {htmlElement} pathToChanginBlock
 *@param {string} removedClass
 */
remove = function (pathToRadio, pathToChanginBlock, removedClass) {
    pathToRadio.forEach(function (radioTabValue, indexRadioTab) {
        pathToChanginBlock[indexRadioTab].classList.remove(removedClass);
    });
};

/**
 *Add class with the name addedClass
 *@param {htmlElement} pathToChanginBlock
 *@param {number} index
 *@param {string} addedClass
 */
active = function (pathToChanginBlock, index, addedClass) {
    pathToChanginBlock[index].classList.add(addedClass);
}

/**
 *Create marcked list
 *return {string} countList
 */
createList = function (list) {
    let countList = '';
    const count = getE('.count-marcked-list').value;
    countList += ` <${list} style="list-style:${markType}"> `;
    for (let i = 0; i < count; i++) {
        countList += ` <li>Текст</li> `;
    }
    countList += `</${list}>`;
    return countList;
}

/**
 *Create table
 *return {string} table
 */
createTable = function () {
    let table = '';
    const countRow = getE('.count-row').value,
        countCell = getE('.count-cell').value,
        heightCell = getE('.height-cell').value,
        widthCell = getE('.width-cell').value,
        widthBorder = getE('.width-border').value;
    table += ' <table> ';
    for (let i = 0; i < countRow; i++) {
        table += `<tr>`;
        for (let j = 0; j < countCell; j++) {
            table += ` <td style="border: ${widthBorder}px ${borderType} ${borderColor}; width:${widthCell}px; height:${heightCell}px"> </td> `;
        }
        table += ' </tr>';
    }
    table += ' </table>';
    return table;
};

/**
Confirmation on validation. It's true if it isn't 'string', 0, bigger then 99 or less then 0.
@param {number} inputValue.
*/
isNumber = function (inputValue) {
    return (!isNaN(inputValue) && !0 && inputValue > 0 && inputValue < 100);
};

/**
 *Change value of count-market-list by click on list-type-radio
 */
function chageValueOfActiveListInput() {
        if (getAll('.list-type-radio')[0].checked) {
            markType = 'disc';
        } else {
            markType = 'decimal';
        }
    getE('.active-list-type select').addEventListener('change', function () {
            markType = this.value;
        });
}


/**
 *Show valid or invalid input block
 *@param {number} inputValue
 *@param {number} index
 */
function showInvalidInput(inputValue, index) {
    if (isNumber(inputValue.value)) {
        inputValue.style.outlineColor = 'green';
        inputValue.parentElement.classList.remove('tooltipMessage');
        return true;
    } else {
        inputValue.style.outlineColor = 'red';
        inputValue.parentElement.classList.add('tooltipMessage');
        return false;
    }
}

/**
 *Check checked tab
 *@return {string} textArea.value
 */
function checkedActiveTabAndCreate() {
    if (getE('.radio-tab').checked) {
        textArea.value += createTable();
    } else if (getE('.list-type-radio').checked) {
        textArea.value += createList('ul');
    } else {
        textArea.value += createList('ol');
    }
    return textArea.value;
}

/**
 *Reset input field by change active tab
 *@param {htmlElement} inputValue
 */
function resetAllInput(inputValue) {
    inputValue.value = null;
    inputValue.style.outlineColor = '#6a707c';
    inputValue.parentElement.classList.remove('tooltipMessage');
    hideElement(['.button-create']);
}

/**
 *Validate and show create button
 *@param {number} countOfInputField
 */
function validateAndShowCreateButton(countOfInputField) {
    let count = 0;
    valid.forEach(function (inputValid, index) {
        if (inputValid) {
            count++;
        }
        if (count === countOfInputField) {
            showElement(['.button-create']);
            //            resetAllInput('.count-marcked-list');
        } else {
            hideElement(['.button-create']);
        }
    });
}

/**
 *Show table or lists in example-window-body
 */
function showTableOrList() {
    if (getE('.radio-tab').checked) {
        getE('.example-window-body').innerHTML = createTable();
    } else if (getE('.list-type-radio').checked) {
        getE('.example-window-body').innerHTML = createList('ul');
    } else {
        getE('.example-window-body').innerHTML = createList('ol');
    }
}

/**
*Passes all input fields in order to gain access to the active field and Change color of outline on 'green', if the
 client input right information, and 'red' if it's wrong.
*/
function passesAllInputInActiveTab() {
    let validRow = [];
    getAll('.active-tab-content input[type=text]').forEach(function (inputActive, index) {
        resetAllInput(inputActive);
        inputActive.addEventListener('input', function () {
            validRow[index] = showInvalidInput(inputActive, index);
            valid = validRow;
            validateAndShowCreateButton(getAll('.active-tab-content input[type=text]').length)
        });
    });
}

/**
 *Copy from content to text-area-block
 */
getE('.button-edit').addEventListener('click', function () {
    showElement(['.editor', '.editor-area']);
    hideElement(['.editor-style-wrap', '.editor-add']);
    textArea.value = getE('.content div').innerHTML;
});

/**
 *Apeeared or disapeared table style by onclick
 */
getE('.button-style').addEventListener('click', function () {
    hideElement(['.editor-area', '.editor-add']);
    showElement(['.editor', '.editor-style-wrap']);
});

/**
 *Copy from content to text-area-block.
 */
getE('.button-save').addEventListener('click', function () {
    text.innerHTML = textArea.value;
});

/**
 *Appeared or disappeared table add by onclick.
 */
getE('.button-add').addEventListener('click', function () {
    showElement(['.editor-add']);
    hideElement(['.editor-area']);
});

/**
 *Hide the block window if password true, by the click on the unblock button
 */
getE('.button-unblock').addEventListener('click', function () {
    validatePassword();
});

/**
 *Show the block window by the click on the block button
 */
getE('.button-block').addEventListener('click', function () {
    showElement(['.password-window']);
    showElement(['.layout']);
});

/**
 *Change the size of text in text-area-block.
 *@param {htmlElement} radioButton
 */
radioBtn.forEach(function (radioButton) {
    radioButton.addEventListener('click', function () {
        text.style.fontSize = this.value;
    });
});

/**
 *Passes all buttons on text-area-block and add text decoration by click on some button
 */
editorStyleButtons.forEach(function (styleButton, index) {
    styleButton.addEventListener('click', function () {
        addStyleByClickOnButton(index);
    });
});

/**
 *Change the font of text in text-area-block.
 */
getE('.text-font select').addEventListener('change', function () {
    text.style.fontFamily = this.value;
});

/**
 *Change the background.
 */
getE('.style-background-picker').addEventListener('change', function () {
    text.style.background = `${getE('.style-background-picker').value}`;
});

/**
 *Change the background.
 */
getE('.style-color-picker').addEventListener('change', function () {
    text.style.color = `${getE('.style-color-picker').value}`;
});

/**
 *Change the font-Weight of text in text-area-block.
 */
getE('.text-bold').addEventListener('change', function () {
    if (this.checked) {
        text.style.fontWeight = 'bold';
    } else {
        text.style.fontWeight = 'normal';
    }
});

/**
 *Change the font-Style of text in text-area-block.
 */
getE('.text-italic').addEventListener('change', function () {
    if (this.checked) {
        text.style.fontStyle = 'italic';
    } else {
        text.style.fontStyle = 'normal';
    }
});

/**
 *Change the text decoration of text in text-area-block.
 */
getE('.text-underline').addEventListener('change', function () {
    if (this.checked) {
        text.style.textDecoration = 'underline';
    } else {
        text.style.textDecoration = 'none';
    }
});

/**
 *Remove and add class "active-tab-content" to div with class "tab-content" for making active div visible
 *@param {htmlElement} pathToRadio
 *@param {number} indexRadioTab
 */
radioTab.forEach(function (pathToRadio, index) {
    passesAllInputInActiveTab();
    pathToRadio.addEventListener('click', function () {
        remove(radioTab, getAll('.tab-content'), 'active-tab-content');
        active(getAll('.tab-content'), index, 'active-tab-content');
        passesAllInputInActiveTab();
    });
});

/**
 *Remove and add class "active-list-type" to div with class "marked-type-block" for making active select visible
 *@param {htmlElement} pathToRadio
 *@param {number} indexRadioTab
 */
getAll('.list-type-radio').forEach(function (pathToRadio, index) {
    pathToRadio.addEventListener('click', function () {
        remove(getAll('.list-type-radio'), getAll('.marked-type-block'), 'active-list-type');
        active(getAll('.marked-type-block'), index, 'active-list-type');
        chageValueOfActiveListInput();
        resetAllInput(getE('.count-marcked-list'));
    });
});

/**
 *Change border-type  by choose.
 */
getE('.border-type select').addEventListener('change', function () {
    borderType = this.value;
});

/**
 *Change border-color  by choose
 */
getE('.border-color input').addEventListener('change', function () {
    borderColor = this.value;
});

/**
 *Show list type field by click on list radio 
 */
getAll('.list-type-radio').forEach(function (checkedType) {
    checkedType.addEventListener('click', function () {
        if (checkedType.checked) {
            showElement(['.fieldset-marked-list-types']);
        }
    });
});

/**
 *Create table or list in text-area-block
 */
getE('.button-create').addEventListener('click', function () {
    hideElement(['.editor-add']);
    showElement(['.content', '.control-panel-wrap', '.editor-area']);
    checkedActiveTabAndCreate();
});

/**
 *By the click on the show example button show example of the table
 */
getE('.button-example').addEventListener('click', function () {
    showTableOrList();
});

/**
 *By the click on close icon hide rxample window 
 */
getE('.example-window-head img').addEventListener('click', function () {
    hideElement(['.example-window']);
    showElement(['.example-window-icon img']);
});

/**
 *By the click on the example window icon show this window 
 */
getE('.example-window-icon img').addEventListener('click', function () {
    showElement(['.example-window']);
    hideElement(['.example-window-icon img']);
    exampleWindow.style.left = `40%`;
    exampleWindow.style.top = null;
});

/**
 *By mousedown on the window make toggleOfMouseClick true for starting move
 */
exampleWindow.addEventListener('mousedown', function (event) {
    isMouseDown = true;
    mouseOffSet = {x: exampleWindow.offsetLeft - event.clientX,
                  y: exampleWindow.offsetTop - event.clientY}
});

/**
 *Mouse move window if toggleOfMouseClick true 
 */
document.addEventListener('mousemove', function (event) {
    event.preventDefault();
    if (isMouseDown) {
        exampleWindow.style.top = event.clientY + mouseOffSet.y + 'px';
        exampleWindow.style.left = event.clientX + mouseOffSet.x + 'px';
    }
});

/**
 *By mousedup on the window make toggleOfMouseClick false for ending move
 */
exampleWindow.addEventListener('mouseup', function () {
    isMouseDown = false;
});
